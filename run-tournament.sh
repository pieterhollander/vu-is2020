#!/bin/bash

#time python3 train-rand-ml-bot.py -d rand.pkl -m rand.pkl
#time python3 train-bully-ml-bot.py -d bully.pkl -m bully.pkl
#time python3 train-rdeep-ml-bot.py -d rdeep.pkl -m rdeep.pkl

time python3 tournament-extra.py -p "rand,bully,rdeep" -r 1000
time python3 tournament-extra.py -p "rand_ml,bully_ml,rdeep_ml" -r 1000
time python3 tournament-extra.py -p "rand_ml,rand" -r 1000
time python3 tournament-extra.py -p "rand_ml,bully" -r 1000
time python3 tournament-extra.py -p "rand_ml,rdeep" -r 1000
time python3 tournament-extra.py -p "bully_ml,rand" -r 1000
time python3 tournament-extra.py -p "bully_ml,bully" -r 1000
time python3 tournament-extra.py -p "bully_ml,rdeep" -r 1000
time python3 tournament-extra.py -p "rdeep_ml,rand" -r 1000
time python3 tournament-extra.py -p "rdeep_ml,bully" -r 1000
time python3 tournament-extra.py -p "rdeep_ml,rdeep" -r 1000
